#README
The packer templates provided here try to be as modular as possible:
* Avoid to 'hard code' the Vagrant environment.
Therefore the entire vagrant configuration is moved to scripts, which can be disabled rather easily
* Have an up-to-date OS

# TL;DR

```
packer build --only=qemu file.json
```

or

```
packer build --only=virtualbox-iso file.json
```

#ToDo
* Ubuntu is not yet working properly: Issues with vagrant user and ssh on 14.04

# The Machines

## CentOS
* All CentOS machines are installed via kickstart. The kickstart file is located at ``kickstart/http``.
* Every machine has 40GiB of hard disk
* The installation is done via the root user
* The vagrant user is added via ``scripts/centos/vagrant``


# Hosting the box locally (versioned)
Create a `.json` file and add the following (or similar) content:

```
{
  "description": "A basic CentOS made by me.",
  "short_description":"CentOS",
  "name": "Thubo/CentOS6",
  "versions": [
    {
      "version": "6.7.20160112",
      "status":"revoked",
      "description_html":null,
      "description_markdown":null,
      "created_at" : "2016-01-12:00:00.000Z",
      "updated_at" : "2016-01-12:00:00.000Z",
      "providers": [
        {
          "checksum": "12345",
          "checksum_type": "md5",
          "name": "virtualbox",
          "url": "http://boxes.example.com/CentOS-6-20160112.virtualbox"
        }
      ]
    },
    {
      "version": "6.7.20160126",
      "status":"active",
      "description_html":null,
      "description_markdown":null,
      "created_at" : "2016-01-26T00:00:00.000Z",
      "updated_at" : "2016-01-26T00:00:00.000Z",
      "providers": [
        {
          "checksum": "12345",
          "checksum_type": "md5",
          "name": "virtualbox",
          "url": "http://boxes.example.com/CentOS-6-20160126.virtualbox"
        }
      ]
    }
  ]
}
```

Make this file available via http(s) and use the box in you Vagrantfile with

```
config.vm.box = "Thubo/CentOS"
config.vm.box_url = "http://boxes.example.com/ftp/boxes/CentOS.json"
config.vm.box_version = "~> 6.7.0"
```

# Credits
My packer templates were inspired by

* https://github.com/shiguredo/packer-templates
* https://github.com/chef/bento

