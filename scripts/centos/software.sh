#!/bin/bash
yum -y groupinstall "development tools"
yum -y install vim wget bzip2 nfs-utils deltarpm yum-utils net-tools wget curl
yum -y install tree rsync git gitflow
# Vagrant Stuff
yum -y install openssh-clients rsync wget curl
