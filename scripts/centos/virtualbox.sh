#!/bin/bash
yum -y install kernel-devel kernel-headers dkms gcc
VBOX_VERSION="$(cat /etc/vbox_version)"
cd /tmp
# wget -q http://download.virtualbox.org/virtualbox/$VBOX_VERSION/VBoxGuestAdditions_$VBOX_VERSION.iso
mount -o loop VBoxGuestAdditions_$VBOX_VERSION.iso /mnt
sh /mnt/VBoxLinuxAdditions.run
umount /mnt
rm VBoxGuestAdditions_$VBOX_VERSION.iso
