#!/bin/bash
echo -n "Disabling security..."
service iptables stop
setenforce 0
chkconfig iptables off
sed -i s/"SELINUX=permissive"/"SELINUX=disabled"/ /etc/selinux/config
sed -i s/"SELINUX=enforcing"/"SELINUX=disabled"/ /etc/selinux/config
echo "done"

echo -n "Modifying /etc/sudoers..."
sed -i "s/^.*requiretty/#Defaults requiretty/" /etc/sudoers
echo "done"
