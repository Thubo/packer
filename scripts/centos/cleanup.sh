#!/bin/bash
yum -q -y erase gtk2 libX11 hicolor-icon-theme avahi freetype bitstream-vera-fonts
echo -n "Cleaning yum..."
yum -q -y clean all
rm -rf /tmp/rubygems-*
echo "done"

# Make sure Udev doesn't block our network
echo -n "Cleaning up udev rules..."

for ifcfg in $(ls /etc/sysconfig/network-scripts/ifcfg-*); do
    if [ "$(basename ${ifcfg})" != "ifcfg-lo" ]; then
      echo -n "$(basename ${ifcfg})..."
      sed -i '/^UUID/d'   ${ifcfg}
      sed -i '/^HWADDR/d' ${ifcfg}
    fi
done

if [[ -e /etc/udev/rules.d/70-persistent-net.rules ]]; then
  rm /etc/udev/rules.d/70-persistent-net.rules
fi

if [[ -e /dev/.udev/ ]]; then
  rm -rf /dev/.udev/
fi

if [[ -e /lib/udev/rules.d/75-persistent-net-generator.rules ]]; then
  rm /lib/udev/rules.d/75-persistent-net-generator.rules
fi

echo "done"
