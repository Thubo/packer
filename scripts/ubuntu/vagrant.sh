#!/bin/bash
# Vagrant specific
date > /etc/vagrant_box_build_time

# Add vagrant user
/usr/sbin/groupadd -g 1000 vagrant
/usr/sbin/useradd vagrant -u 1000 -g vagrant
echo "vagrant"|passwd --stdin vagrant
echo "Defaults !requiretty" >> /etc/sudoers.d/vagrant
echo "vagrant        ALL=(ALL)       NOPASSWD: ALL" >> /etc/sudoers.d/vagrant
chmod 0440 /etc/sudoers.d/vagrant

# Installing vagrant keys
mkdir -pm 700 /home/vagrant/.ssh
wget -q --no-check-certificate \
  'https://github.com/mitchellh/vagrant/raw/master/keys/vagrant.pub' \
  -O /home/vagrant/.ssh/authorized_keys
chown -R vagrant:vagrant /home/vagrant/.ssh
chmod -R go-rwsx /home/vagrant/.ssh
