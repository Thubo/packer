#!/bin/bash
apt-get -y autoremove
apt-get -y clean

echo "Cleaning up dhcp leases"
rm /var/lib/dhcp/*

echo "Cleaning up udev rules"
rm /etc/udev/rules.d/70-persistent-net.rules
mkdir /etc/udev/rules.d/70-persistent-net.rules
rm -rf /dev/.udev/
rm /lib/udev/rules.d/75-persistent-net-generator.rules

