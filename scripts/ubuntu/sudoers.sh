#!/bin/bash
echo -n "Disabling security..."
service iptables stop
setenforce 0
chkconfig iptables off
sed -i s/"SELINUX=permissive"/"SELINUX=disabled"/ /etc/selinux/config
sed -i s/"SELINUX=enforcing"/"SELINUX=disabled"/ /etc/selinux/config
echo "done"

sed -i -e '/Defaults\s\+env_reset/a Defaults\texempt_group=sudo' /etc/sudoers
sed -i -e 's/%sudo  ALL=(ALL:ALL) ALL/%sudo  ALL=NOPASSWD:ALL/g' /etc/sudoers
