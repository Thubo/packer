#!/bin/bash
# Make ssh faster by not waiting on DNS
echo -n "Modifying sshd_config..."
echo "UseDNS no" >> /etc/ssh/sshd_config
echo "GSSAPIAuthentication no" >> /etc/ssh/sshd_config
echo "done"
