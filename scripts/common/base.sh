#!/bin/bash
# Make /etc/hosts world readable
echo -n "Making /etc/hosts world readable..."
chmod 644 /etc/hosts
echo "done"
